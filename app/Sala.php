<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Sala
 *
 * @property $id
 * @property $nombre
 * @property $sala
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Sala extends Model
{
    
    static $rules = [
		'nombre' => 'required',
		'estado' => 'required'
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre', 'estado'];
    public function medicos() {
        return $this->hasMany(Medico::class,'sala_id' , 'id');
    }


}
