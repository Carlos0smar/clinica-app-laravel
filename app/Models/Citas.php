<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Citas extends Model
{
    use HasFactory;
    protected $fillable =[
        'fecha_mes',
        'fechaCita',
        'estado',
        'medico_id',
    ];

    public function pacientes() {
        return $this->belongsTo(Citas::class,'medico_id','id');
    }
}
