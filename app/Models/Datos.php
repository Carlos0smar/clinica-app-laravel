<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Datos extends Model
{
    use HasFactory;
    protected $fillable=[
        'altura',
        'peso',
        'num_emergencia',
        'tipo_sangre',
        'alergia'
    ];
}
