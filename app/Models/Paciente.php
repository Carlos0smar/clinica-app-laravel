<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    use HasFactory;
    protected $fillable =[
        'nombre' ,
        'apellido',
        'edad' ,
        'genero' ,
        'telefono' ,
        'direccion' ,
        'email',
    ];
    
    public function citas(){
        return $this->hasMany(Citas::class,'medico_id','id');
    }
}
