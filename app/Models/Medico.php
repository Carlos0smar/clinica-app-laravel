<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medico extends Model
{
    use HasFactory;
    protected $fillable=[
        'nombre' ,
        'apellido',
        'edad' ,
        'telefono' ,
        'informacion' ,
        'especialidad',
        'email',
        'sala_id'
        ];
    
    public function sala()
    {
        return $this->belongsTo(Sala::class,'sala_id' , 'id');
    }
}
