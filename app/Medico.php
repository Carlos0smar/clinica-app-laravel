<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


/**
 * Class Medico
 *
 * @property $id
 * @property $nombre
 * @property $apellido
 * @property $edad
 * @property $telefono
 * @property $informacion
 * @property $especialidad
 * @property $email
 * @property $sala_id
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Medico extends Model
{
    
    static $rules = [
		'nombre' => 'required',
		'apellido' => 'required',
		'edad' => 'required',
		'telefono' => 'required',
		'informacion' => 'required',
		'especialidad' => 'required',
		'email' => 'required',
    'sala_id' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre','apellido','edad','telefono','informacion','especialidad','email','sala_id'];

    public function sala()
    {
        return $this->belongsTo(Sala::class,'sala_id' , 'id');
    }


}
