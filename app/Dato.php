<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Dato
 *
 * @property $id
 * @property $altura
 * @property $peso
 * @property $num_emergencia
 * @property $tipo_sangre
 * @property $alergia
 * @property $paciente_id
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Dato extends Model
{
    
    static $rules = [
		'altura' => 'required',
		'peso' => 'required',
		'num_emergencia' => 'required',
		'tipo_sangre' => 'required',
		'alergia' => 'required',
		'paciente_id' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['altura','peso','num_emergencia','tipo_sangre','alergia','paciente_id'];



}

