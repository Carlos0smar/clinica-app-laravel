<?php

namespace App\Http\Controllers;

use App\Models\Citas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CitasController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {   
        $medico = DB::select('SELECT id, nombre, apellido FROM medicos WHERE especialidad = ?', ['General']);
        $table = DB::select('SELECT fechaCita as hora, fecha_mes as dia FROM citas');

        return view('citas.sacar_ficha', ['medico' => $medico], ['table' => $table]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // $medico = DB::select('SELECT id, nombre, apellido FROM medicos WHERE especialidad = ?', ['General']);
        // $table = DB::select('SELECT fechaCita as hora, fecha_mes as dia FROM citas');
        
        // return view('citas.sacar_ficha', ['medico' => $medico], ['table' => $table]);
    }

    /**
     * Store a newly created resource in storage.
     * 
     */
    public function store(Request $request)
    {
        
        $listo =$request->all();
        Citas::create($listo);
        // dd($listo);
        
        return redirect()->route('citas.index')
        ->with('success', 'La cita fue registrada correctamente.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Citas $citas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Citas $citas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Citas $citas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Citas $citas)
    {
        //
    }

}
