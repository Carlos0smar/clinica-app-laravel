<?php

namespace App\Http\Controllers;

use App\Medico;
use Illuminate\Http\Request;
use App\Sala;

/**
 * Class MedicoController
 * @package App\Http\Controllers
 */
class MedicoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $medicos = Medico::paginate(10);

        return view('medico.index', compact('medicos'))
            ->with('i', (request()->input('page', 1) - 1) * $medicos->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $medico = new Medico();
        $salas = Sala::where('estado', 'si')->get();
        return view('medico.create', compact('medico', 'salas'));
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Medico::$rules);
    
        $medico = Medico::create($request->all());
    
        $salaId = $request->input('sala_id');
        Sala::where('id', $salaId)->update(['estado' => 'no']);
    
        return redirect()->route('medico.index')
            ->with('success', 'Médico creado exitosamente.');
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $medico = Medico::find($id);

        return view('medico.show', compact('medico'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $medico = Medico::find($id);
        $salas = Sala::where('estado', 'si')->get();
        return view('medico.edit', compact('medico', 'salas'));
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Medico $medico
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Medico $medico)
    {
        // Valida los datos del formulario
        $request->validate(Medico::$rules);
    
        // Obtiene el ID de la sala seleccionada del formulario
        $salaId = $request->input('sala_id');
    
        // Actualiza la sala en función del ID seleccionado
        Sala::where('id', $salaId)->update(['estado' => 'no']);
    
        // Actualiza los demás datos del médico
        $medico->update($request->all());
        return redirect()->route('medico.index')->with('success', 'Médico actualizado exitosamente');
    }
    

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        // Busca y elimina al médico
        $medico = Medico::find($id);
        $medico->delete();
    
        // Recupera el ID de la sala asociada al médico (si existe)
        $salaId = $medico->sala_id;
    
        // Si se encontró un ID de sala asociado, actualiza su estado a "si"
        if (!is_null($salaId)) {
            Sala::where('id', $salaId)->update(['estado' => 'si']);
        }
    
        return redirect()->route('medico.index')
            ->with('success', 'Médico eliminado exitosamente');
    }
    
}

