<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Medico>
 */
class MedicoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {   
        $especialidades = ['Cardiología', 'Dermatología', 'Oftalmología', 'Cirugía', 'Ginecología', 'Ortopedia', 'Neurología', 'Radiología', 'General'];
        return [
            'nombre' => fake()->firstName(),
            'apellido' => fake()->lastName(),
            'edad' => fake()->numberBetween(24, 60),
            'telefono' => fake()->randomNumber(8, true),
            'informacion' => fake()->text(100),
            'especialidad' => fake()->randomElement($especialidades),
            'email' => fake()->email(),
            'sala_id' => fake()->numberBetween(1,20),
        ];
    }
}
