<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Sala;

class SalaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        
        Sala::create(
            [
                'id' => 1,
                'nombre' => 'C-100'
            ]);
            Sala::create([
                'id' => 2,
                'nombre' => 'C-200'
            ]);
            Sala::create([
                'id' => 3,
                'nombre' => 'C-300'
            ]);
            Sala::create([
                'id' => 4,
                'nombre' => 'C-400'
            ]);
            Sala::create([
                'id' => 5,
                'nombre' => 'C-500'
            ]);
            Sala::create([
                'id' => 6,
                'nombre' => 'C-600'
            ]);
            Sala::create([
                'id' => 7,
                'nombre' => 'C-700'
            ]);
            Sala::create([
                'id' => 8,
                'nombre' => 'C-800'
            ]);
            Sala::create([
                'id' => 9,
                'nombre' => 'C-900'
            ]);
            Sala::create([
                'id' => 10,
                'nombre' => 'C-1000'
            ]);
            Sala::create([
                'id' => 11,
                'nombre' => 'D-100'
            ]);
            Sala::create([
                'id' => 12,
                'nombre' => 'D-200'
            ]);
            Sala::create([
                'id' => 13,
                'nombre' => 'D-300'
            ]);
            Sala::create([
                'id' => 14,
                'nombre' => 'D-400'
            ]);
            Sala::create([
                'id' => 15,
                'nombre' => 'D-500'
            ]);
            Sala::create([
                'id' => 16,
                'nombre' => 'D-600'
            ]);
            Sala::create([
                'id' => 17,
                'nombre' => 'D-700'
            ]);
            Sala::create([
                'id' => 18,
                'nombre' => 'D-800'
            ]);
            Sala::create([
                'id' => 19,
                'nombre' => 'D-900'
            ]);
            Sala::create([
                'id' => 20,
                'nombre' => 'D-1000'
            ]);
            Sala::create([
                'id' => 21,
                'nombre' => 'E-100'
            ]);
            Sala::create([
                'id' => 22,
                'nombre' => 'E-200'
            ]);
            Sala::create([
                'id' => 23,
                'nombre' => 'E-300'
            ]);
            Sala::create([
                'id' => 24,
                'nombre' => 'E-400'
            ]);
            Sala::create([
                'id' => 25,
                'nombre' => 'E-500'
            ]);
            Sala::create([
                'id' => 26,
                'nombre' => 'E-600'
            ]);
            Sala::create([
                'id' => 27,
                'nombre' => 'E-700'
            ]);
            Sala::create([
                'id' => 28,
                'nombre' => 'E-800'
            ]);
            Sala::create([
                'id' => 29,
                'nombre' => 'E-900'
            ]);
            Sala::create([
                'id' => 30,
                'nombre' => 'E-1000'
            ]);
            Sala::create([
                'id' => 31,
                'nombre' => 'A-100'
            ]);
            Sala::create([
                'id' => 32,
                'nombre' => 'A-200'
            ]);
            Sala::create([
                'id' => 33,
                'nombre' => 'A-300'
            ]);
            Sala::create([
                'id' => 34,
                'nombre' => 'A-400'
            ]);
            Sala::create([
                'id' => 35,
                'nombre' => 'A-500'
            ]);
    }
}
