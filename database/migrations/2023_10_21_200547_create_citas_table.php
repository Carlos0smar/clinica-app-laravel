<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('citas', function (Blueprint $table) {
            $table->id();
            $table->time('fechaCita');
            $table->date('fecha_mes');
            $table->enum('estado', ['Disponible','Ocupado'])->default('Ocupado');
            $table->unsignedBigInteger('medico_id');
            $table->foreign('medico_id')->references('id')->on('medico')->name('fk_medi_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('citas');
    }
};
