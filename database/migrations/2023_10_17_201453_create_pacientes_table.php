<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pacientes', function (Blueprint $table) {
            $table->id();
            // $table->string('password',30);
            $table->string('nombre',30);
            $table->string('apellido',30);
            $table->enum('genero',['M', 'F', 'Otro'])->default('Otro');
            $table->string('direccion',70);
            $table->integer('telefono');
            $table->string('email');
            // $table->string('nivel', 10)->default('Paciente');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pacientes');
    }
};
