<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('medicos', function (Blueprint $table) {
            $table->id();
            // $table->string('password',30);
            $table->string('nombre',30);
            $table->string('apellido',30);
            $table->integer('edad');
            $table->integer('telefono');
            $table->string('informacion',200);
            $table->string('especialidad');
            $table->string('email');
            // $table->string('nivel', 10)->default('Medico');
            $table->unsignedBigInteger('sala_id')->nullable();
            $table->foreign('sala_id')->references('id')->on('salas')->name('fk_sala');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('medicos');
    }
};
