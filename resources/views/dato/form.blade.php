<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('altura') }}
            {{ Form::text('altura', $dato->altura, ['class' => 'form-control' . ($errors->has('altura') ? ' is-invalid' : ''), 'placeholder' => 'Altura']) }}
            {!! $errors->first('altura', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('peso') }}
            {{ Form::text('peso', $dato->peso, ['class' => 'form-control' . ($errors->has('peso') ? ' is-invalid' : ''), 'placeholder' => 'Peso']) }}
            {!! $errors->first('peso', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('num_emergencia') }}
            {{ Form::text('num_emergencia', $dato->num_emergencia, ['class' => 'form-control' . ($errors->has('num_emergencia') ? ' is-invalid' : ''), 'placeholder' => 'Num Emergencia']) }}
            {!! $errors->first('num_emergencia', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('tipo_sangre') }}
            {{ Form::text('tipo_sangre', $dato->tipo_sangre, ['class' => 'form-control' . ($errors->has('tipo_sangre') ? ' is-invalid' : ''), 'placeholder' => 'Tipo Sangre']) }}
            {!! $errors->first('tipo_sangre', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('alergia') }}
            {{ Form::text('alergia', $dato->alergia, ['class' => 'form-control' . ($errors->has('alergia') ? ' is-invalid' : ''), 'placeholder' => 'Alergia']) }}
            {!! $errors->first('alergia', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('paciente_id') }}
            {{ Form::text('paciente_id', $dato->paciente_id, ['class' => 'form-control' . ($errors->has('paciente_id') ? ' is-invalid' : ''), 'placeholder' => 'Paciente Id']) }}
            {!! $errors->first('paciente_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
    </div>
</div>