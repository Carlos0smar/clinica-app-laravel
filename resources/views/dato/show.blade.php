@extends('layouts.app')

@section('template_title')
    {{ $dato->name ?? "{{ __('Show') Dato" }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">{{ __('Show') }} Dato</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('datos.index') }}"> {{ __('Back') }}</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Altura:</strong>
                            {{ $dato->altura }}
                        </div>
                        <div class="form-group">
                            <strong>Peso:</strong>
                            {{ $dato->peso }}
                        </div>
                        <div class="form-group">
                            <strong>Num Emergencia:</strong>
                            {{ $dato->num_emergencia }}
                        </div>
                        <div class="form-group">
                            <strong>Tipo Sangre:</strong>
                            {{ $dato->tipo_sangre }}
                        </div>
                        <div class="form-group">
                            <strong>Alergia:</strong>
                            {{ $dato->alergia }}
                        </div>
                        <div class="form-group">
                            <strong>Paciente Id:</strong>
                            {{ $dato->paciente_id }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
