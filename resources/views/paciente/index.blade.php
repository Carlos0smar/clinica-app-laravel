@extends('layouts.app')

@section('template_title')
    Paciente
@endsection
        <style>
            .pagination {
                display: flex;
                flex-direction: row;
                justify-content: center;
                align-items: center;
                margin-right: 10px;
            }

            ul.pagination > li {
                display: inline-block;
            }

            ul.pagination > li > a {
                margin-right: 10px;
            }

            ul.pagination > li > a span {
                margin-left: 5px;
            }
            .btn-primary{
                background: #283E61;;
                border: none;
                padding: .50em;
                width: 90px;
                height: 30px;
                margin: 0 auto;
                color: #fff;
                text-align: center;
                border-radius: 25px;

            }
        </style>

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Paciente') }}
                            </span>

                             <div class="float-right">
                                <a href="{{ route('paciente.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Create New') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>No</th>
                                        
										<th>Nombre</th>
										<th>Apellido</th>
										<th>Genero</th>
										<th>Direccion</th>
										<th>Telefono</th>
										<th>Email</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($pacientes as $paciente)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            
											<td>{{ $paciente->nombre }}</td>
											<td>{{ $paciente->apellido }}</td>
											<td>{{ $paciente->genero }}</td>
											<td>{{ $paciente->direccion }}</td>
											<td>{{ $paciente->telefono }}</td>
											<td>{{ $paciente->email }}</td>

                                            <td>
                                                <form action="{{ route('paciente.destroy',$paciente->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('paciente.show',$paciente->id) }}"><i class="fa fa-fw fa-eye"></i> {{ __('Mostra') }}</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('paciente.edit',$paciente->id) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Editar') }}</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> {{ __('Eliminar') }}</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="pagination">
                        <ul class="pagination">
                        @if ($pacientes->currentPage() > 1)
                        <li><a href="{{ $pacientes->previousPageUrl() }}" class="btn-primary">&laquo; Anterior</a></li>
                        @endif

                        @for ($i = 1; $i <= $pacientes->lastPage(); $i++)
                        <li class="{{ $i == $pacientes->currentPage() ? 'active' : '' }}">
                            <a href="{{ $pacientes->url($i) }}" class="btn-primary">{{ $i }}</a>
                        </li>
                        @endfor

                        @if ($pacientes->hasMorePages())
                        <li><a href="{{ $pacientes->nextPageUrl() }}" class="btn-primary">Siguiente &raquo;</a></li>
                        @endif
                        </ul>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
@endsection
