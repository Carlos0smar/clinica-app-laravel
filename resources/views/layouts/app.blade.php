<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  ================================================== -->
  <meta charset="utf-8">
  <title>Novena- Health Care &amp; Medical template</title>

  <!-- Mobile Specific Metas
  ================================================== -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="Health Care Medical Html5 Template">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0">
  <meta name="author" content="Themefisher">
  <meta name="generator" content="Themefisher Novena HTML Template v1.0">

  <!-- Favicon -->

  <!-- 
  Essential stylesheets
  =====================================-->
  <!-- Main Stylesheet -->
  @vite(['resources/sass/app.scss', 'resources/js/app.js'])


</head>

<body id="top">

<header>
	<div class="header-top-bar">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-6">
					<ul class="top-bar-info list-inline-item pl-0 mb-0">
						<li class="list-inline-item"><a href="mailto:support@gmail.com"><i class="icofont-support-faq mr-2"></i>soporte@novena.com</a></li>
						<li class="list-inline-item"><i class="icofont-location-pin mr-2"></i>La dirección Ta-134/A, Nueva York, Estados Unidos.</li>
					</ul>
				</div>
				<div class="col-lg-6">
					<div class="text-lg-right top-right-bar mt-2 mt-lg-0">
						<a href="tel:+23-345-67890">
							<span>Llama Ahora : </span>
							<span class="h4">823-4565-13456</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<nav class="navbar navbar-expand-lg navigation" id="navbar">
		<div class="container">
			<a class="navbar-brand" href="index.html">
				<img src="images/logo.png" alt="" class="img-fluid">
			</a>

			<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarmain"
				aria-controls="navbarmain" aria-expanded="false" aria-label="Toggle navigation">
				<span class="icofont-navigation-menu"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarmain">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active"><a class="nav-link" href="{{url('/')}}">Inicio</a></li>
					<li class="nav-item"><a class="nav-link" href="{{route('citas.index')}}">Ficha</a></li>
							<!-- <li class="nav-item"><a class="nav-link" href="{{route('citas.create')}}">Ficha</a></li> -->
							<!-- <li class="nav-item"><a class="nav-link" href="javascript:cargarContenido('Create_ficha_interface.php')">Ficha</a></li> -->

							<li class="nav-item"><a class="nav-link" href="#)">Citas</a></li>
							<!-- <li class="nav-item"><a class="nav-link" href="javascript:cargarContenido('Create_ficha_interface.php')">Ficha</a></li> -->

							<li class="nav-item"><a class="nav-link" href="{{route('medico.index')}}">Medicos</a></li>

							<li class="nav-item"><a class="nav-link" href="#">Citas Agendadas</a></li>

							<li class="nav-item"><a class="nav-link" href="{{route('paciente.index')}}">Pacientes</a></li>
						<div class="btn-container ">

							</div>
					</div>
				</ul>
			</div>
		</div>
	</nav>
</header>


<div class="container"id="contenido">
@yield('content')
</div>

<!-- footer Start -->
<footer class="footer section gray-bg">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 mr-auto col-sm-6">
				<div class="widget mb-5 mb-lg-0">
					<div class="logo mb-4">
						<img src="images/logo.png" alt="" class="img-fluid">
					</div>
					<p>Siguenos en nuestras redes sociales:</p>

					<ul class="list-inline footer-socials mt-4">
						<li class="list-inline-item">
							<a href="https://www.facebook.com/themefisher"><i class="icofont-facebook"></i></a>
						</li>
						<li class="list-inline-item">
							<a href="https://twitter.com/themefisher"><i class="icofont-twitter"></i></a>
						</li>
						<li class="list-inline-item">
							<a href="https://www.pinterest.com/themefisher/"><i class="icofont-linkedin"></i></a>
						</li>
					</ul>
				</div>
			</div>

			<div class="col-lg-2 col-md-6 col-sm-6">
				<div class="widget mb-5 mb-lg-0">
					<h4 class="text-capitalize mb-3">Departmento</h4>
					<div class="divider mb-4"></div>

					<ul class="list-unstyled footer-menu lh-35">
						<li><a href="#!">Cirugía </a></li>
						<li><a href="#!">Cuidado de la mujer</a></li>
						<li><a href="#!">Radiología</a></li>
						<li><a href="#!">Cardiología</a></li>
						<li><a href="#!">Medicina</a></li>
					</ul>
				</div>
			</div>

			<div class="col-lg-2 col-md-6 col-sm-6">
				<div class="widget mb-5 mb-lg-0">
					<h4 class="text-capitalize mb-3">Soporte</h4>
					<div class="divider mb-4"></div>

					<ul class="list-unstyled footer-menu lh-35">
						<li><a href="#!">Términos y condiciones</a></li>
						<li><a href="#!">Política de privacidad</a></li>
						<li><a href="#!">Soporte de la empresa </a></li>
						<li><a href="#!">Preguntas frecuentes (FAQ)</a></li>
						<li><a href="#!">Licencia de la empresa</a></li>
					</ul>
				</div>
			</div>

			<div class="col-lg-3 col-md-6 col-sm-6">
				<div class="widget widget-contact mb-5 mb-lg-0">
					<h4 class="text-capitalize mb-3">Contáctanos</h4>
					<div class="divider mb-4"></div>

					<div class="footer-contact-block mb-4">
						<div class="icon d-flex align-items-center">
							<i class="icofont-email mr-3"></i>
							<span class="h6 mb-0">Soporte Disponible 24/7</span>
						</div>
						<h4 class="mt-2"><a href="mailto:support@email.com">soporte@email.com</a></h4>
					</div>

					<div class="footer-contact-block">
						<div class="icon d-flex align-items-center">
							<i class="icofont-support mr-3"></i>
							<span class="h6 mb-0">Lunes a Viernes : 08:30 - 18:00</span>
						</div>
						<h4 class="mt-2"><a href="tel:+23-345-67890">+23-456-6588</a></h4>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>



  </body>
  </html>