@extends('layouts.app')

@section('template_title')
    Medico
@endsection
        <style>
            .pagination {
                display: flex;
                flex-direction: row;
                justify-content: center;
                align-items: center;
                margin-right: 10px;
            }

            ul.pagination > li {
                display: inline-block;
            }

            ul.pagination > li > a {
                margin-right: 10px;
            }

            ul.pagination > li > a span {
                margin-left: 5px;
            }
            .btn-primary{
                background: #283E61;;
                border: none;
                padding: .50em;
                width: 90px;
                height: 30px;
                margin: 0 auto;
                color: #fff;
                text-align: center;
                border-radius: 25px;

            }
        </style>


@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Medico') }}
                            </span>

                             <div class="float-right">
                                <a href="{{ route('medico.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Crear Nuevo') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>No</th>
                                        
										<th>Nombre</th>
										<th>Apellido</th>
										<th>Edad</th>
										<th>Telefono</th>
										<th>Informacion</th>
										<th>Especialidad</th>
										<th>Email</th>
                                        <th>Sala</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($medicos as $medico)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            
											<td>{{ $medico->nombre }}</td>
											<td>{{ $medico->apellido }}</td>
											<td>{{ $medico->edad }}</td>
											<td>{{ $medico->telefono }}</td>
											<td>{{ $medico->informacion }}</td>
											<td>{{ $medico->especialidad }}</td>
											<td>{{ $medico->email }}</td>
                                            <td>{{ $medico->sala->nombre }}</td>

                                            <td>
                                                <form action="{{ route('medico.destroy',$medico->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('medico.show',$medico->id) }}"><i class="fa fa-fw fa-eye"></i> {{ __('Mostrar') }}</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('medico.edit',$medico->id) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Editar') }}</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> {{ __('Eliminar') }}</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="pagination">
                        <ul class="pagination">
                        @if ($medicos->currentPage() > 1)
                        <li><a href="{{ $medicos->previousPageUrl() }}" class="btn-primary">&laquo; Anterior</a></li>
                        @endif

                        @for ($i = 1; $i <= $medicos->lastPage(); $i++)
                        <li class="{{ $i == $medicos->currentPage() ? 'active' : '' }}">
                            <a href="{{ $medicos->url($i) }}" class="btn-primary">{{ $i }}</a>
                        </li>
                        @endfor

                        @if ($medicos->hasMorePages())
                        <li><a href="{{ $medicos->nextPageUrl() }}" class="btn-primary">Siguiente &raquo;</a></li>
                        @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection
