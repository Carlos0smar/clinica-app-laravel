@extends('layouts.app')

@section('template_title')
    {{ $medico->name ?? "{{ __('Mostrar') Medico" }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">{{ __('Mostrar') }} Medico</span>
                        </div>
                        
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Nombre:</strong>
                            {{ $medico->nombre }}
                        </div>
                        <div class="form-group">
                            <strong>Apellido:</strong>
                            {{ $medico->apellido }}
                        </div>
                        <div class="form-group">
                            <strong>Edad:</strong>
                            {{ $medico->edad }}
                        </div>
                        <div class="form-group">
                            <strong>Telefono:</strong>
                            {{ $medico->telefono }}
                        </div>
                        <div class="form-group">
                            <strong>Informacion:</strong>
                            {{ $medico->informacion }}
                        </div>
                        <div class="form-group">
                            <strong>Especialidad:</strong>
                            {{ $medico->especialidad }}
                        </div>
                        <div class="form-group">
                            <strong>Email:</strong>
                            {{ $medico->email }}
                        </div>
                        <div class="form-group">
                            <strong>Sala:</strong>
                            {{ $medico->sala->nombre }}
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('medico.index') }}"> {{ __('Atras') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
