@extends('layouts.app')

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reserva en el Hospital</title>
    
    <!-- Enlace al archivo de estilos de Bootstrap -->
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.5.0/dist/css/bootstrap.min.css" rel="stylesheet"> -->
    <!-- Estilos personalizados -->
    <style>
        body {
            background-color: #f8f9fa;
        }
/* 
        .btn btn-primary{
            background-color:  #E6F0F6 !important;
        } */
        .card {
            border: none;sssss
            border-radius: 10px;
            box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.1);
            overflow: hidden;
        }
        .card-header {
            display: flex;
            /* justify-content: space-between; */
            background-color: #283E61 !important;
            color: white;
            text-align: center;
            padding: 15px;
            border-radius: 10px 10px 2 2;
            
        }
        .card-body {
            padding: 15px;
          
        }
        .form-label {
            font-weight: bold;
            text-align: center; /* Si deseas centrar el contenido dentro del contenedor */
            
        }
        .form-control {
            border-radius: 8px;
            border: 1px solid #ced4da;
            padding: 10px;
        }
        .form-select {
            border-radius: 8px;
            border: 1px solid #ced4da;
            padding: 10px;
        }
        .btn-primary {
            background-color: #283E61 !important;
            border: none !important;
            border-radius: 20px !important;
            padding: 10px 20px !important;
            margin-top: 20px !important;
                   
        }
        .contenedor {
         
           margin-top: 20px;
        }
        .contenedor2 {
			/* color: white; */
            border: none;
            border-radius: 10px;
            box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.1);
            border-radius: 8px;
            padding: 10px 50px;
            
            background-color:  #E6F0F6;
          
         }
         .contenedor3 {
            margin-top: 40px;
            color: #283E61;
            
         }
         
         .contenedor-principal {
            position: relative;
            width: 400px;
            height: 300px;
            background-color: #f0f0f0;
        }

        table{
            border-collapse: collapse;
            width: 100%;
            margin-top: 20px;
            color: white;

        }
        table thead{
            background-color: #283E61;
        }
        table, th, td {
        border: 1px solid #ccc;
        }

        th, td {
        padding: 10px;
        text-align: center;
        }

        .hora{
            background-color: #283E61;
        }
        button {
        width: 100%;
        height: 40px;
        background-color: white;
        border: 1px none;
        cursor: pointer;
        border-radius: 15px; 
        }

        button.red {
        background-color: red;
        }

        .contenedor-superpuesto:nth-child(2) {
            top: 43%;
            left: 35%;
            background-color: rgba(255, 255, 255, 0.8);
            padding: 60px;
            border: 0px solid #ccc;
        
        } 
        .contenedor-superpuesto2 {
            display: flex;
            align-items: center;
            /* position: relative; */
            /* top: 8.5%; */
            /* left: 10%; */
            /* transform: translate(-50%, -50%); */
            text-align: center;
            background-color: rgba(255, 255, 255, 0.8);
            padding: 10px;
            border: 1px solid #ccc;
        }
    
        .contenedorIm {
         width: 300px;
         height: 300px;
        }
        .contenedorIm2 {
         width: 100px;
         height: 20px;
         margin-right: 20px;
      }
    </style>
    <!-- <script src="js/fetch.js"></script> -->
</head>
<body>
@section('content')
	<div class="container">
		<div class="col">
			<div class="card">

				<div class="card-header">
					<h2 class="mb-0" style="text-align: center; color: white;" >Reservas</h2>
					
				</div>

				<div class="card-body">
					<form method="POST" action= "{{ route('citas.store')}}" id="formFicha">
                        @csrf
						<div class="contenedorIm">
							<img src="images\22.jpg" alt="Mi Imagen"  width="1350" height="300">
						</div>
                        @if(session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                        @endif
						<div class="contenedor-superpuesto">
							<div class="contenedor3">
								<label for="dia" class="form-label" required>Día:</label>
                                <select class="form-select" id="dia" name="fecha_mes" required ></select>
                                <p id="fecha-seleccionada"></p>

							</div>
                            <div class="contenedor3">
								<label for="hora" class="form-label" required>Hora:</label>
                                <select class="form-select" id="hora" name="fechaCita"  ></select>

                            </div>
							<div class="contenedor3">
								<label for="medico" class="form-label" required>Médico</label>
								<select class="form-select" id="id_medico" name="medico_id" required>
								</select>
							</div>
							
							<div class="contenedor3">
								<button type="submit" class="btn btn-primary">hacer cita</button> 
							</div>
						</div>
					</form>
				</div>

				<div class="contenedor2">
					<span> <h3 sytle="color: white !important;">Horario</h3></span>

					<div class="contenedor3" id="tabla_horarios">
                    </div>

				</div>
			</div>
		</div>
	</div>
    <script>
        var medico_ = @json($medico);
        var table_ = @json($table);
    </script>
@endsection

</body>
</html>
