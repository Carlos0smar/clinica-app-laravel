<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CitasController;
use App\Http\Controllers\PacienteController;
use App\Http\Controllers\DatoController;
use App\Http\Controllers\MedicoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('inde');
});
Route::resource('paciente',PacienteController::class);
Route::resource('dato',DatoController::class);
Route::get('dato/{id}', [DatoController::class,'create'])->name('dato.create');
Route::resource('medico',MedicoController::class);
Route::resource('citas',CitasController::class);